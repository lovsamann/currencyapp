//
//  ViewController.m
//  PeerReviewWeek3
//
//  Created by Ann on 9/30/15.
//  Copyright © 2015 Ann. All rights reserved.
//

#import "ViewController.h"
#import "CurrencyRequest/CRCurrencyRequest.h"
#import "CurrencyRequest/CRCurrencyResults.h"

@interface ViewController ()<CRCurrencyRequestDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UILabel *firstCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *thridCurrencyLabel;
@property (weak, nonatomic) IBOutlet UIButton *convertButton;
@property (nonatomic) CRCurrencyRequest *req;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)convertButtonClicked:(UIButton *)sender {
    self.convertButton.enabled = NO;
    
    self.req = [[CRCurrencyRequest alloc]init];
    self.req.delegate = self;
    [self.req start];
    
    self.convertButton.enabled = YES;
}

- (void)currencyRequest:(CRCurrencyRequest *)req
    retrievedCurrencies:(CRCurrencyResults *)currencies{
    
    self.convertButton.enabled = YES;
    
    self.firstCurrencyLabel.text = [NSString stringWithFormat:@"%.2f ¥", [self.inputField.text floatValue] * currencies.JPY];
    self.secondCurrencyLabel.text = [NSString stringWithFormat:@"%.2f ₨", [self.inputField.text floatValue] * currencies.INR];
    self.thridCurrencyLabel.text = [NSString stringWithFormat:@"%.2f ₽", [self.inputField.text floatValue] * currencies.MXN];
}



@end
